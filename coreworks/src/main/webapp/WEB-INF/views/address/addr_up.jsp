<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/address_menu.jsp" />
<!-- date picker css -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_mj.css">
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>연락처 수정</h2></div>
                    <!-- ** 코드 작성부 시작 -->
                    <div class="main_cnt">
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">주소록 구분 *</div>
                            <div class="main_cnt_desc">
                                <select>
                                    <option value="" hidden>선택</option>
                                    <option value="">공용 주소록</option>
                                    <option value="">내 주소록</option>
                                </select>
                            </div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">그룹 구분</div>
                            <div class="main_cnt_desc">
                                <select>
                                    <option value="">공통</option>
                                    <option value="">오렌지레드</option>
                                </select>
                            </div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">아이디</div>
                            <div class="main_cnt_desc"><input type="text" name="" id="" value=""></div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">이름 *</div>
                            <div class="main_cnt_desc"><input type="text" name="" id="" value=""></div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">회사명</div>
                            <div class="main_cnt_desc"><input type="text" name="" id="" value=""></div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">직책</div>
                            <div class="main_cnt_desc"><input type="text" name="" id="" value=""></div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">직급</div>
                            <div class="main_cnt_desc"><input type="text" name="" id="" value=""></div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">부서</div>
                            <div class="main_cnt_desc"><input type="text" name="" id="" value=""></div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">핸드폰번호</div>
                            <div class="main_cnt_desc"><input type="text" name="" id="" value=""></div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">이메일</div>
                            <div class="main_cnt_desc"><input type="text" name="" id="" value=""></div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">회사주소</div>
                            <div class="main_cnt_desc">
                                <!-- 주소 검색 폼 시작 -->
                            	<div class="address_form_wrap">
	                           		<div class=""><input type="text" name="" id="postcode_form" placeholder="우편번호" readonly><a href="#" class="button btn_pink" id="address_srch_btn" onclick="DaumPostcode();">검색</a></div>
                            		<div class=""><input type="text" name="" id="address_form" placeholder="주소" readonly></div>
                            		<div class=""><input type="text" name="" id="detailAddress_form" placeholder="상세주소"></div>
                            	</div>
                                <!-- 주소 검색 폼 끝 -->
                            </div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <input type="checkbox" id="spe_day"><label for="spe_day">기념일 사용</label>
                            <div class="spe_day_wrap">
                                <ul class="spe_day_ctn"></ul>
                                <button class="btn_main spe_day_add_btn">추가</button>
                            </div>
                        </div>
                        <div class="main_cnt_list clearfix main_cnt_list_btn">
                            <button class="btn_pink">취소</button>
                            <button class="btn_main">수정</button>
                        </div>
                    </div>
                    <!-- ** 코드 작성부 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>

<!-- 주소검색 api -->
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<!-- datepicker api -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(3).addClass("on");

        // 기념일
        (function(){
            var use_spe_day = true; // 추후 기념일 있는지 없는 지 판별 후 넣기
            var use_spe_day_count = 3; // 기존 기념일 개수

            var node_count = 1;
            var real_node = 1; // 실제 기념일 개수
            var nodeArr = new Array(); // 기념일 속성 idx 판별 위한 배열, 추후 자바 쪽에서 이용
            var base_node = "<li class='spe_day_list'><div class='spe_day_tit'><input type='text' name='' id='' class='spe_name' placeholder='기념일명을 입력하세요.'><input type='text' name='' id='' class='datepicker' placeholder='날짜를 선택하세요.'></div><div class='spe_day_date_type'><input type='radio' name='' class='solar' checked><label for='' class='solar_txt'>양력</label><input type='radio' name='' id='' class='lunar'><label for='' class='lunar_txt'>음력</label></div><div class='spe_day_date_btn'><button class='btn_white'>삭제</button></div></li>";
            
            // 기존 기념일이 있다면
            if(use_spe_day) {
                $("#spe_day").prop("checked", true);
                $(".spe_day_wrap").show();

                for(var i = 1; i <= use_spe_day_count; i++){

                    if(i != 1){
                        node_count++;
                        real_node++;
                    }

                    nodeArr[node_count - 1] = 1;
                    $(".spe_day_ctn").append(base_node);
                    $(".spe_day_ctn").find(".spe_day_list").eq(real_node - 1).find(".spe_name").eq(0).prop("name", "spe_name" + node_count);
                    $(".spe_day_ctn").find(".spe_day_list").eq(real_node - 1).find(".solar").eq(0).prop({
                                                                                                            "name" : "cal_type" + node_count,
                                                                                                            "id" : "solar" + node_count
                                                                                                        }).siblings(".solar_txt").prop("for", "solar"+node_count);
                    $(".spe_day_ctn").find(".spe_day_list").eq(real_node - 1).find(".lunar").eq(0).prop({
                                                                                                            "name" : "cal_type" + node_count,
                                                                                                            "id" : "lunar" + node_count
                                                                                                        }).siblings(".lunar_txt").prop("for", "lunar"+node_count);
                    
                    $(".spe_day_ctn").find(".spe_day_list").eq(real_node - 1).find(".datepicker").eq(0).prop("id", "datepicker" + node_count);
                    $(".spe_day_ctn").find(".spe_day_list").eq(real_node - 1).find(".datepicker").eq(0).datepicker({
                        changeMonth: true,
                        changeYear: true,
                        nextText: '다음 달',
                        prevText: '이전 달',
                        dateFormat: "yy/mm/dd",
                        showMonthAfterYear: true , 
                        dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
                        monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
                    });
                    
                }
                console.log("node_count:"+node_count);
                console.log("real_node:"+real_node);
                console.log("nodeArr:"+nodeArr);
            }
            

            $("#spe_day").on("change", function(){
                if($(this).prop("checked") == true){
                    $(".spe_day_wrap").show();
                    $(".spe_day_ctn").append(base_node);
                    $(".spe_day_ctn").find(".spe_day_list").eq(0).find(".datepicker").eq(0).prop("id", "datepicker1").datepicker({
                        changeMonth: true,
                        changeYear: true,
                        nextText: '다음 달',
                        prevText: '이전 달',
                        dateFormat: "yy/mm/dd",
                        showMonthAfterYear: true , 
                        dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
                        monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
                    });
                        
                    $(".spe_day_ctn").find(".spe_day_list").eq(0).find(".solar").eq(0).prop({
                                                                                                "name" : "cal_type1",
                                                                                                "id" : "solar1"
                                                                                            }).siblings(".solar_txt").prop("for", "solar1");
                    $(".spe_day_ctn").find(".spe_day_list").eq(0).find(".lunar").eq(0).prop({
                                                                                                "name" : "cal_type1",
                                                                                                "id" : "lunar1"
                                                                                            }).siblings(".lunar_txt").prop("for", "lunar1");
                    nodeArr[0] = 1;
                }else{
                    var spe_use_yn = confirm("기념일 사용을 해제하시면 기존의 기념일이 삭제됩니다.");
                    if(spe_use_yn){

                        $(".spe_day_wrap").hide();
                        $(".spe_day_ctn").find(".spe_day_list").remove();
                        node_count = 1;
                        real_node = 1;
                        nodeArr = new Array();
                    } else{
                        $(this).prop("checked", true);
                        return false;
                    }
                }
                console.log("node_count:"+node_count);
                console.log("real_node:"+real_node);
                console.log("nodeArr:"+nodeArr);
            });


            $(".spe_day_add_btn").on("click", function(){
                node_count++;
                real_node++;
                nodeArr[node_count - 1] = 1;
                $(".spe_day_ctn").append(base_node);
                $(".spe_day_ctn").find(".spe_day_list").eq(real_node - 1).find(".spe_name").eq(0).prop("name", "spe_name" + node_count);
                $(".spe_day_ctn").find(".spe_day_list").eq(real_node - 1).find(".solar").eq(0).prop({
                                                                                                        "name" : "cal_type" + node_count,
                                                                                                        "id" : "solar" + node_count
                                                                                                    }).siblings(".solar_txt").prop("for", "solar"+node_count);
                $(".spe_day_ctn").find(".spe_day_list").eq(real_node - 1).find(".lunar").eq(0).prop({
                                                                                                        "name" : "cal_type" + node_count,
                                                                                                        "id" : "lunar" + node_count
                                                                                                    }).siblings(".lunar_txt").prop("for", "lunar"+node_count);
                
                $(".spe_day_ctn").find(".spe_day_list").eq(real_node - 1).find(".datepicker").eq(0).prop("id", "datepicker" + node_count);
                $(".spe_day_ctn").find(".spe_day_list").eq(real_node - 1).find(".datepicker").eq(0).datepicker({
                    changeMonth: true,
                    changeYear: true,
                    nextText: '다음 달',
                    prevText: '이전 달',
                    dateFormat: "yy/mm/dd",
                    showMonthAfterYear: true , 
                    dayNamesMin: ['월', '화', '수', '목', '금', '토', '일'],
                    monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월']
                });
                
                console.log("node_count:"+node_count);
                console.log("real_node:"+real_node);
                console.log("nodeArr:"+nodeArr);
            });

            $(document).on("click", ".spe_day_date_btn button", function(){
                var list_count = $(".spe_day_ctn").find(".spe_day_list").length;

                if(list_count > 1){
                    real_node--;
                    $(this).parents(".spe_day_list").remove();
                    var node_idx = $(this).parents(".spe_day_list").find(".datepicker").prop("id").split("datepicker")[1];
                    nodeArr[node_idx - 1] = 0;
                }else{
                    alert("기념일은 최소 1개 이상 등록해야합니다.");
                }
                
                console.log("node_count:"+node_count);
                console.log("real_node:"+real_node);
                console.log("nodeArr:"+nodeArr);
            });


        })();
    });

    
    // 주소 검색 시작
    function DaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                var addr = ''; // 주소 변수
                var extraAddr = ''; // 참고항목 변수

                addr = data.roadAddress;
                
                if(data.bname !== '' && /[동|로|가]$/g.test(data.bname)){
                    extraAddr += data.bname;
                }
                if(data.buildingName !== '' && data.apartment === 'Y'){
                    extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                if(extraAddr !== ''){
                    extraAddr = ' (' + extraAddr + ')';
                }

                document.getElementById('postcode_form').value = data.zonecode;
                document.getElementById("address_form").value = addr;
                document.getElementById("detailAddress_form").focus();
            }
        }).open();
    }

    $("#postcode_form, #address_form").on("click", function(){
        $("#address_srch_btn").trigger("click");
    });
    // 주소 검색 끝
</script>
</body>
</html>