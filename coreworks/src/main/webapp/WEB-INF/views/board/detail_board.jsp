<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%--
	공지사항 상세보기
--%>

<jsp:include page="../inc/board_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_sh.css">

		<div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>공지사항 작성</h2></div>
                    
                    <!--  작성자 혹은 관리자의 경우 삭제나 수정가능
                    <div class="btn_set">
                        <button class="btn_pink">삭제</button>
                    	<button class="btn_blue">수정</button>
                    </div>
                    -->
                    <div class="title">
						<span class="must_read">필독</span>
						<span>인장관리규정 신설 및 시행 안내</span>
					</div>						            
						
					<div class="board_info">
						<span>이원경</span>
						<span>조회수 123</span>
						<span>등록일 20/01/15</span>
					</div>
					
					<!-- 
						내용넣는 칸
						html째로 넣음 
					-->
					<div class="fr-view">
						<p><span style="white-space:pre-wrap;">안녕하세요 총무부 부장 이원경입니다.<br><br>내부통제 개선 사항으로 인장관리규정이 별도로 신설되어 안내 드리오니<br>첨부된 인장관리규정 확인을 하셔서 업무에 참고하시기 바랍니다.<br><br>신설된 인장관리규정에서 업무상 빈번히 발생하는 인장 날인 및 반출 전결 규정이 변경 되었습니다.<br>변경된 전결 규정을 준수해 주시기 바랍니다.<br><br>감사합니다.<br><br>◎ 인장관리규정 시행일 : 2020년 1월 1일<br><br>◎ 인장 관리 전결 규정</span> <img src="https://s3-eu-west-1.amazonaws.com/froala-eu/temp_files%2F1579084406766-image+1.png" style="width: 300px;" class="fr-fic fr-dib fr-fil"></p>
						<div class="att_info">
							<span>첨부파일</span>
							<span>인장관리규정2020V1.pdf (214.1KB)</span>
							<span><button class="btn_white btn_save">저장</button></span>
						</div>
					</div>
					
					<!-- 댓글 공간 -->
					<div class="comment_area">
						<div class="comment_count">댓글 3개</div>
						
						<!-- 해당 컨테이너에 댓글답글 전부 포함 -->
						<div class="comment_and_reply">
							<div class="comment">
								<div class="comment_wt">
									<span>최원준</span>
									<span class="time">02:12 PM</span>
								</div>
								<div class="comment_content">확인했습니다.</div>
								<button class="btn_white btn_reply">답글</button>
								<!-- 작성자 혹은 관리자일 시 답글제거가능 -->
								<!-- <button class="btn_pink btn_comment_delete">삭제</button> -->
								<!-- <button class="btn_blue btn_comment_update">수정</button> -->
							</div>
						</div>
						
						<!-- 댓글&답글 -->
						<div class="comment_and_reply">
							<!-- 댓글 -->
							<div class="comment">
								<div class="comment_wt">
									<span>문혜린</span>
									<span class="time">02:12 PM</span>
								</div>
								<div class="comment_content">첨부파일이 이상합니다.</div>
								<button class="btn_white btn_reply">답글</button>
							</div> <!-- 댓글 -->
							

							<!-- 답글 -->
							<div class="reply">
							<span><i class="fas fa-long-arrow-alt-right"></i></span>
							<div class="reply_wt">
								<span>이원경</span>
								<span class="time">02:12 PM</span>
								</div>
								<div class="reply_content">수정했습니다.</div>
								<!-- 작성자일 시 답글제거가능 -->
								<!-- <button class="btn_white btn_reply_delete">삭제</button> -->
								<!-- <button class="btn_white btn_reply_update">수정</button> -->
							</div> <!-- 답글 -->
						</div> <!-- 댓글&답글 -->
						
						<!-- 댓글 작성란 -->
						<form action="" >
							<div class="write_comment_area">
								<textarea class="insert_comment" placeholder="댓글을 입력하세요."></textarea>
								<div class="btn_area">
									<button class="btn_pink btn_comment_add">등록</button>
								</div>
							</div> 
						</form><!-- 댓글 작성란 -->
						
					</div> <!-- 댓글 공간 -->
					
					</div>         
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>

<!-- 답글 폼 -->
<div class="reply reply_ex" hidden>
<span><i class="fas fa-long-arrow-alt-right"></i></span>
		<div class="reply_wt">
			<span>이원경</span>
		</div>
		<div class="write_comment_area write_reply_area">
			<textarea class="insert_comment" placeholder="답글을 입력하세요."></textarea>
			<div class="btn_area reply_btn_area">
				<button class="btn_white btn_reply_cancel">취소</button>
				<button class="btn_blue btn_reply_add">저장</button>
			</div>
		</div>
</div>
<script>
$(function(){
    // 주 메뉴 분홍색 하이라이트 처리
    $("#nav .nav_list").eq(5).addClass("on");

    // 서브 메뉴 처리
    // 열리지 않는 메뉴
    //$("#menu_area .menu_list").eq(0).addClass("on");
    
    // 열리는 메뉴
    $("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
    $("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");
    
    // 서브메뉴 밑줄제거
    $('#container #menu_area .menu_list.on>a').css('text-decoration', 'none');
    
    // 답글버튼 클릭 시 답글전용 작성공간 생성
    /*
		<div class="write_comment_area write_reply_area">
			<textarea class="insert_comment" placeholder="답글을 입력하세요."></textarea>
			<div class="btn_area reply_btn_area">
				<button class="btn_white btn_reply_cancel">취소</button>
				<button class="btn_blue btn_reply_add">저장</button>
			</div>
		</div>
    */
    
    // 답글생성
    $(document).on("click",".btn_reply",function(event){
    	const write_reply_area = $('.reply_ex').clone().removeAttr('hidden');
    	
    	$(this).parents('.comment_and_reply').append(write_reply_area);
    	
    	$(this).remove();
    });
    
    // 답글취소 클릭 시 작성 폼 제거
    $(document).on("click",".btn_reply_cancel",function(event){
    	$(this).parents('.comment_and_reply').children('.comment').append('<button class="btn_white btn_reply">답글</button>');
    	$(this).parents('.reply_ex').remove();
    	
    });
    
    // 댓글 등록
    $('.btn_comment_add').click(function() {
    	if($(this).parents('.write_comment_area').children('.insert_comment').val() === '') {
    		return false;
    	} else {
    		alert('댓글이 등록되었습니다.');
    		return true;
    	}
    	
    })
});

</script>
</body>
</html>