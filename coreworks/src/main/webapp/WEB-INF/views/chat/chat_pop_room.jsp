<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


<div class="chat-popup" id="chatForm"> <!-- 채팅창 -->
	<form action="" class="form-container" enctype="multipart/form-data"> <!-- 채팅 서버 전송을 위한 form -->
		<div class="chattopmenu"> <!-- 채팅방 상단 뒤로가기, x버튼 -->
			<div class="xbtn" id="whitexbtn">
				<i class="fas fa-times"></i>
			</div>
		</div>
		
		<div class="chattarget"> <!-- 채팅방 상단 이미지, 이름, 파일 저장함 -->
			<div class="chatuserimg">
                <img src="${ contextPath }/resources/images/user_img.jpg" alt="">
            </div>
            <div class="chatuser">
            <!-- 1:1 채팅일 때 이름과 소속 출력 -->
	        <!--     <div class="chatusername">
	            	황규환
	            </div>
	            <div class="chatuserdept">
	            	소속
	            </div> -->
	           	<!-- 1:1 아니면, 채팅방 인원 이름 출력, 혹은 채팅방 이름 설정 가능 ? -->
	           	<div class="multichatusername">
	           		황규환, 김민석
	           	</div>
            </div>
            <div class="filebox" id="filebox">
            	<i class="fas fa-inbox"></i>
            	<a href="#filebox_pop" rel="modal:open" id="pop"></a>
            </div>
		</div>
		<div class="chathistory"> <!-- 채팅 내역 나오는 부분 -->
			<div class="mychat">
				<div class="innerchattime">
					09:16 PM
				</div>
				<div class="chatbox">
				가나다라마바사아자차카타파하가나다라마바사아자차카타파하
				</div>
			</div>
			<div class="yourchat"> <!-- 상대방 채팅 -->
				<div class="chatbox">
					23456761111111111111111111111111111111111111111111111111111
				</div>
				<div class="innerchattime">
					09:16 PM
				</div>
			</div>
			<div class="mychat"> <!-- 내채팅 -->
				<div class="innerchattime">
					09:16 PM
				</div>
				<div class="chatbox">
				가나다라마바사아
				</div>
			</div>
			<div class="yourchat"> <!-- 상대방이 이미지 보냈을 때 -->
				<div class="chatbox">
					<img src="${ contextPath }/resources/images/user_img.jpg" />
				</div>
				<div class="innerchattime">
					09:16 PM
				</div>
			</div>
			<div class="mychat"> <!-- 내가 이미지 보낼 때 -->
				<div class="innerchattime">
					09:16 PM
				</div>
				<div class="chatbox">
					<img src="${ contextPath }/resources/images/testimg.jpg" />
				</div>
			</div>
		</div>
		<textarea name="msg" id="msg" required></textarea> <!-- 메세지 전송할 부분 -->
		<div class="chatlastdiv"> <!-- 파일선택, 메세지 전송 버튼 -->
			<div class="filebtn">
				<i class="fas fa-paperclip"></i>
				<input type="file" id="filebtn" hidden/>
			</div>
			<div class="sendbtn">
				<i class="fab fa-telegram-plane"></i>
				<button type="submit" id="sub" hidden></button>
			</div>
		</div>
	</form>
</div>

<!-- popup include -->
<div id="filebox_pop" class="modal">
	<jsp:include page="../pop/filebox_pop.jsp" />
</div>


<script>
<%-- 상단 x버튼 클릭시 채팅방 닫기 --%>
$(".chattopmenu .xbtn").click(function() { 
	$("#chatForm").css("display","none");
});

<%-- 채팅 인풋박스에서 엔터 입력시 메세지 전송, esc 입력시 채팅방 닫기 --%>
$(document).on("keydown", "#msg", function(e) {
	if(e.keyCode == 13) {
		if(!event.shiftKey) {
			e.preventDefault();
			$("#sub").click();
		}
	} else if(e.keyCode == 27) {
		$("#chatForm").css("display","none");
	}
});

<%-- 클립버튼 클릭 시 파일 첨부 열기 --%>
$(document).on("click", ".filebtn > svg" ,function() {
 	$("#filebtn").eq(0).trigger("click");
});

<%-- 비행기버튼 클릭 시 메세지 전송 기능 --%>
$(document).on("click", ".sendbtn > svg", function() {
	$("#sub").click();
});

<%-- 파일박스 클릭시 기존에 전송했던 파일들 목록 보기 --%>
$(document).on("click", ".filebox > svg" ,function() {
 	$("#pop").trigger("click");
});

</script>