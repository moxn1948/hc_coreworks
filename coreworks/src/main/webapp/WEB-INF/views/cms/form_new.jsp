<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<jsp:include page="../inc/cms_menu.jsp" />
<link href='https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/css/froala_editor.pkgd.min.css' rel='stylesheet' type='text/css' />
<script type='text/javascript' src='https://cdn.jsdelivr.net/npm/froala-editor@3.1.0/js/froala_editor.pkgd.min.js'></script>
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_wj.css">
    
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->

                <div class="main_ctn">
						
                    <div class="menu_tit"><h2>기안서식 추가</h2></div>
                   
                     <!-- ** 코드 작성부 시작 -->
                    <div class="main_cnt">
                            <div class="main_cnt_tit">폴더</div>
                        <div class="main_cnt_list clearfix">
                            <select name="" id="">
                            <option value="">공통</option>
                            <option value="">2</option>
                       		 </select> 
                        </div>
                        <div class="main_cnt_list clearfix2">                      	
                            <div class="main_cnt_tit">양식명</div>
                            <input type="text" name="" id="">
                        	
                        	<div class="tit">
                            <div class="main_cnt_tit">보존연한</div>
                            <select name="" id="">
                            <option value="1">1년</option>
                            <option value="3">3년</option>
                            <option value="5">5년</option>
                            </select>
                            <input type="checkbox" name="change" id="change" value="변경금지">
							<label for="change">변경금지</label>

                            </div> 	 
                        </div>
                       
                       		<div class="main_cnt_list clearfix">
                           <div class="main_cnt_tit">설명</div>
                            <input type="text" name="" id=""> 	    
                       		</div>
                       		
                       
                       	<div class="main_cnt_list clearfix">
                           <div class="main_cnt_tit">양식형태</div>
                           <div class="box_radio">
						<input type="radio" name="form" id="provide" value="provide" checked>
						<label for="provide">기본제공 양식</label> 
						<input type="radio" name="form" id="self" value="self">
						<label for="self">직접 편집</label>
						   </div>
							<div class="box_cnt">
								<ul class="list">
									<li>하단 툴을 활용해 결재 문서 본문의 폰트, 정렬, 이미지 첨부, 표 삽입 등을 사용할 수 있습니다</li>
									<li>디자인을 통한 고급 편집 기능 외 HTML/TEXT 기능도 사용할 수 있습니다.</li>
									<li>MS OFFICE나 한글 파일을 복사해 에디터에 붙여 넣기를 하면 원본 파일 서식과 100% 일치하지 않을 수 있습니다. 그럴 때는 사용자가 에디터의 소스 보기를 통해 수정을 해주시거나 에디터에서 직접 작성해 주셔야 합니다.</li>
								</ul>
							</div>
                      	</div>
                           
                        <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn align">
                                <tr>
                                    <td class="tit"><input type="checkbox" name="check1" id="check1" value="1"></td>
                                    <td class="tit align">이름</td>
                                    <td class="tit align">ID</td>
                                    <td class="tit align">부서</td>
                                    <td class="tit align">연차</td>
                                    <td class="tit align">추가 연차</td>
                                    <td class="tit align">추가 일수</td>
                			   </tr>
                               <tr>
                               		<td></td>
                               		<td></td>
                               		<td></td>
                               		<td></td>
                               		<td></td>
                               		<td></td>
                               		<td></td>
                               		
                               </tr>

                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->    
                            
                           
                            
                        
                      
                               
                            </div>
                          
                    </div>
                    <!-- ** 코드 작성부 끝 -->
                   
                    <!-- 에디터 -->
						<div class="fr-view"></div>
            <!-- 메인 컨텐츠 영역 끝! -->
		
                    <div>
                   <a href="#" class="button btn_blue add" onclick="topAdd()">추가</a>
                    </div>
		<!-- inner_rt end -->
        </div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>

	
	
    $(function(){
    	var editor = new FroalaEditor('.fr-view')
    	$(".fr-view").hide();
    	
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(7).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(1).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(0).addClass("on");
        
        $("#provide").click(function() {
        	$(".fr-view").hide();
        	$(".tbl_common.tbl_basic").show();
        	
        });
        $("#self").click(function() {
        	$(".fr-view").show();
        	$(".tbl_common.tbl_basic").hide();
        });
        
    });
    
</script>
</body>
</html>