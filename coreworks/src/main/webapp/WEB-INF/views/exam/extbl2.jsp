<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/ex_group_menu.jsp" />
    
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_mj.css">
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>내 결재완료</h2></div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                        <select name="" id="">
                            <option value="">1</option>
                            <option value="">2</option>
                        </select>
                        <input type="text" name="" id="">
                        <input type="password" name="" id="">
                        <button class="btn_solid">글쓰기</button>
                        <button class="btn_main">버튼</button>
                        <button class="btn_white">버튼</button>
                        <button class="btn_blue">버튼</button>
                        <button class="btn_pink">버튼</button>
                        <a href="#" class="button btn_solid">글쓰기</a>
                        <a href="#" class="button btn_main">a버튼</a>
                        <a href="#" class="button btn_white">a버튼</a>
                        <a href="#" class="button btn_blue">a버튼</a>
                        <a href="#" class="button btn_pink">a버튼</a>
                        <!-- 색 있는 버튼 -->
                        <button class="btn_solid_main">버튼</button>
                        <button class="btn_solid_white">버튼</button>
                        <button class="btn_solid_blue">버튼</button>
                        <button class="btn_solid_pink">버튼</button>

                        <a href="#" class="button btn_solid_main">버튼</a>
                        <a href="#" class="button btn_solid_white">버튼</a>
                        <a href="#" class="button btn_solid_blue">버튼</a>
                        <a href="#" class="button btn_solid_pink">버튼</a>
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn">
                                <colgroup>
                                    <col style="width: *;">
                                    <col style="width: 16%;">
                                </colgroup>
                                <tr class="tbl_main_tit">
                                    <th>제목없으면 이 tr을 삭제</th>
                                    <th>삭제삭제</th>
                                </tr>
                                <tr>
                                    <td class="tit"><a href="#popex" rel="modal:open">Open Modal</a></td>
                                    <td><a href="#">내용5</a></td>
                                </tr>
                                <tr>
                                    <td class="tit"><a href="#">뱁새가 밥을 안머거요</a></td>
                                    <td><a href="#">내용5</a></td>
                                </tr>
                                <tr>
                                    <td class="tit"><a href="#">뱁새가 밥을 안머거요</a></td>
                                    <td><a href="#">내용5</a></td>
                                </tr>
                                <tr>
                                    <td class="tit"><a href="#">내용1</a></td>
                                    <td><a href="#">내용5</a></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                    <div class="pager_wrap">
                        <ul class="pager_cnt clearfix">
                        <li class="pager_com pager_arr first"><a href="javascrpt: void(0);">&#x003C;&#x003C;</a></li>
                        <li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">1</a></li>
                        <li class="pager_com pager_num on"><a href="javascrpt: void(0);">2</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">3</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">4</a></li>
                        <li class="pager_com spager_num"><a href="javascrpt: void(0);">5</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">6</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">7</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">8</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">9</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">10</a></li>
                        <li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
                        <li class="pager_com pager_arr end"><a href="javascrpt: void(0);">&#x003E;&#x003E;</a></li>
                        </ul>
                    </div>
                    <!-- 페이저 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>

<!-- popup include -->
<div id="popex" class="modal">
	<jsp:include page="../pop/pop.jsp" />
</div>

<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(0).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        var path = '${ contextPath }';

        // 열리는 메뉴
        $("#menu_area .menu_list").eq(1).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(0).addClass("on");

        // 첫번째 줄은 앞에 eq 1개, 두번째 줄은 앞쪽부터 eq 2개 수정 : 두 줄 다 맨 뒤에 eq(0) 수정 금지
        $("#menu_area .menu_list").eq(1).find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');
        $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(0).addClass("on").find("a").eq(0).css("background-image", 'url('+path+'/resources/images/w_folder_open.svg)');

    });
</script>
</body>
</html>