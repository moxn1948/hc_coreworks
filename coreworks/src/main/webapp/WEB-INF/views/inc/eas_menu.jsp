<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="./header.jsp" />
<main id="container">
    <div class="full_ct">
        <div class="inner_lt">
            <div class="menu_wrap">
                <div class="menu_btn_wrap">
                    <button class="btn_solid">새 결재 진행</button>
                </div>
                <div id="menu_area" class="tree_shape">
                    <ul class="menu_ctn clearfix">
                        <li class="menu_list add">
                            <a href="#">내 결재</a>
                            <ul class="sub_menu_ctn">
                                <li class="sub_menu_list">
                                    <a href="#">결재대기</a>
                                </li>                              
                                <li class="sub_menu_list">
                                	<a href="#">결재진행</a>
                                </li>
                                <li class="sub_menu_list">
                                	<a href="#">결재반려</a>
                                </li>
                                <li class="sub_menu_list">
                                	<a href="#">임시보관함</a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu_list add">
                            <a href="#">참여 결재</a>
                            <ul class="sub_menu_ctn">
                                <li class="sub_menu_list"><a href="#">결재대기</a></li>
                                <li class="sub_menu_list"><a href="#">결재진행</a></li>
                                <li class="sub_menu_list"><a href="#">결재반려</a></li>
                            </ul>
                        </li>
                        <li class="menu_list add">
                            <a href="#">공람</a>
                            <ul class="sub_menu_ctn">
                                <li class="sub_menu_list"><a href="#">공람대기</a></li>
                                <li class="sub_menu_list"><a href="#">공람완료</a></li>
                                <li class="sub_menu_list"><a href="#">보낸공람</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div><!-- inner_lt end -->
