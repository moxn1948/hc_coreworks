<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="./header.jsp" />

 <main id="container">
    <div class="full_ct">
        <div class="inner_lt">
            <div class="menu_wrap">
                <div id="menu_area">
                    <div class="menu_area_tit">알림</div>
                    <ul class="menu_ctn clearfix">
                        <li class="menu_list"><a href="#">전체 알림</a></li>
                        <li class="menu_list"><a href="#">안읽은 알림</a></li>
                    </ul>
                </div>
            </div>
        </div><!-- inner_lt end -->