<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/main_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_mj.css">
<div id="scroll_area" class="inner_rt">
	<!-- 메인 컨텐츠 영역 시작! -->
	<div class="main_ctn">
		<!-- ** 코드 작성부 시작 -->
		<div class="main_cnt">
			<div class="top_wrap">
				<div class="notice_wrap contents_wrap">
					<p class="tit">알림</p>
					<ul class="cnt">
						<li class="clearfix list read">
							<a href="#" class="cnt_tit ellipsis">[댓글 등록] ‘[폼-판매자] YellowGreen Company - 배송완료처리요청' 댓글이
								등록되었습니다.</a>
							<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
							<p class="cnt_time">09 : 20 PM</p>
						</li>
						<li class="clearfix list read">
							<a href="#" class="cnt_tit ellipsis">[댓글 등록] ‘[폼-판매자] YellowGreen Company - 배송완료처리요청' 댓글이
								등록되었습니다.</a>
							<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
							<p class="cnt_time">19/01/04</p>
						</li>
						<li class="clearfix list read">
							<a href="#" class="cnt_tit ellipsis">[댓글 등록] ‘[폼-판매자] YellowGreen Company - 배송완료처리요청' 댓글이
								등록되었습니다.</a>
							<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
							<p class="cnt_time">19/01/04</p>
						</li>
						<li class="clearfix list">
							<a href="#" class="cnt_tit ellipsis">[댓글 등록] ‘[폼-판매자] YellowGreen Company - 배송완료처리요청' 댓글이
								등록되었습니다.</a>
							<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
							<p class="cnt_time">19/01/04</p>
						</li>
						<li class="clearfix list">
							<a href="#" class="cnt_tit ellipsis">[댓글 등록] ‘[폼-판매자] YellowGreen Company - 배송완료처리요청' 댓글이
								등록되었습니다.</a>
							<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
							<p class="cnt_time">19/01/04</p>
						</li>
					</ul>
				</div>
			</div>
			<div class="bottom_wrap clearfix">
				<div class="bottom_lt_wrap">
					<div class="eas_wait_wrap contents_wrap">
						<p class="tit">결재대기</p>
						<ul class="cnt">
							<li class="clearfix list">
								<a href="#" class="cnt_tit ellipsis">그룹웨어 수정의 건</a>
								<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
								<p class="cnt_time">19/01/04</p>
							</li>
							<li class="clearfix list">
								<a href="#" class="cnt_tit ellipsis">그룹웨어 수정의 건</a>
								<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
								<p class="cnt_time">19/01/04</p>
							</li>
							<li class="clearfix list">
								<a href="#" class="cnt_tit ellipsis">그룹웨어 수정의 건</a>
								<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
								<p class="cnt_time">19/01/04</p>
							</li>
							<li class="clearfix list">
								<a href="#" class="cnt_tit ellipsis">그룹웨어 수정의 건</a>
								<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
								<p class="cnt_time">19/01/04</p>
							</li>
							<li class="clearfix list">
								<a href="#" class="cnt_tit ellipsis">그룹웨어 수정의 건</a>
								<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
								<p class="cnt_time">19/01/04</p>
							</li>
						</ul>
					</div>
					<div class="company_bd_wrap contents_wrap">
						<p class="tit">사내 공지사항</p>
						<ul class="cnt">
							<li class="clearfix list">
								<a href="#" class="cnt_tit ellipsis">이데일리 2020 신년음악회 임직원 초청 안내</a>
								<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
								<p class="cnt_time">19/01/04</p>
							</li>
							<li class="clearfix list">
								<a href="#" class="cnt_tit ellipsis">이데일리 2020 신년음악회 임직원 초청 안내</a>
								<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
								<p class="cnt_time">19/01/04</p>
							</li>
							<li class="clearfix list">
								<a href="#" class="cnt_tit ellipsis">이데일리 2020 신년음악회 임직원 초청 안내</a>
								<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
								<p class="cnt_time">19/01/04</p>
							</li>
							<li class="clearfix list">
								<a href="#" class="cnt_tit ellipsis">이데일리 2020 신년음악회 임직원 초청 안내</a>
								<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
								<p class="cnt_time">19/01/04</p>
							</li>
							<li class="clearfix list">
								<a href="#" class="cnt_tit ellipsis">이데일리 2020 신년음악회 임직원 초청 안내</a>
								<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
								<p class="cnt_time">19/01/04</p>
							</li>
						</ul>
					</div>
				</div>
				<div class="bottom_rt_wrap">
					<div class="mail_wrap contents_wrap">
						<p class="tit">메일</p>
						<ul class="cnt">
							<li class="clearfix list">
								<a href="#" class="cnt_tit ellipsis">이거 처리해주세요 대리님!</a>
								<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
								<p class="cnt_time">19/01/04</p>
							</li>
							<li class="clearfix list">
								<a href="#" class="cnt_tit ellipsis">이거 처리해주세요 대리님!</a>
								<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
								<p class="cnt_time">19/01/04</p>
							</li>
							<li class="clearfix list">
								<a href="#" class="cnt_tit ellipsis">이거 처리해주세요 대리님!</a>
								<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
								<p class="cnt_time">19/01/04</p>
							</li>
							<li class="clearfix list">
								<a href="#" class="cnt_tit ellipsis">이거 처리해주세요 대리님!</a>
								<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
								<p class="cnt_time">19/01/04</p>
							</li>
							<li class="clearfix list">
								<a href="#" class="cnt_tit ellipsis">이거 처리해주세요 대리님!</a>
								<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
								<p class="cnt_time">19/01/04</p>
							</li>
						</ul>
					</div>
					<div class="team_bd_wrap contents_wrap">
						<p class="tit">팀 공지사항</p>
						<ul class="cnt">
							<li class="clearfix list">
								<a href="#" class="cnt_tit ellipsis">이원경의 신년 인사가 있겠습니다.</a>
								<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
								<p class="cnt_time">19/01/04</p>
							</li>
							<li class="clearfix list read">
								<a href="#" class="cnt_tit ellipsis">이원경의 신년 인사가 있겠습니다.</a>
								<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
								<p class="cnt_time">19/01/04</p>
							</li>
							<li class="clearfix list">
								<a href="#" class="cnt_tit ellipsis">이원경의 신년 인사가 있겠습니다.</a>
								<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
								<p class="cnt_time">19/01/04</p>
							</li>
							<li class="clearfix list">
								<a href="#" class="cnt_tit ellipsis">이원경의 신년 인사가 있겠습니다.</a>
								<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
								<p class="cnt_time">19/01/04</p>
							</li>
							<li class="clearfix list">
								<a href="#" class="cnt_tit ellipsis">이원경의 신년 인사가 있겠습니다.</a>
								<a href="#pop_addr" rel="modal:open" class="cnt_name">김민석</a>
								<p class="cnt_time">19/01/04</p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- ** 코드 작성부 끝 -->
	</div>
	<!-- 메인 컨텐츠 영역 끝! -->
</div><!-- inner_rt end -->
</div>
</main>
</div>

<!-- 추후 주소록 팝업 include -->
<div id="pop_addr" class="modal">
	<jsp:include page="../pop/pop.jsp" />
	주소록팝업
</div>


<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
	$(function () {
		// 캘린더 api
        $("#main_menu_cal").jqxCalendar({width: 218, height: 220});

		// 현재시간 표시하는 함수
		printClock();

	});

	// 현재시간 표시하는 함수
	function printClock() {

		var clock = document.getElementById("main_now_time");
		var currentDate = new Date(); // 현재시간
		var calendar = currentDate.getFullYear() + "-" + (currentDate.getMonth() + 1) + "-" + currentDate.getDate() // 현재 날짜
		var amPm = 'AM'; 
		var currentHours = addZeros(currentDate.getHours(), 2);
		var currentMinute = addZeros(currentDate.getMinutes(), 2);
		var currentSeconds = addZeros(currentDate.getSeconds(), 2);

		if (currentHours >= 12) { // 시간이 12보다 클 때 PM으로 세팅, 12를 빼줌
			amPm = 'PM';
			currentHours = addZeros(currentHours - 12, 2);
		}

		clock.innerHTML = currentHours + " : " + currentMinute + " : " + currentSeconds +
			" " + amPm; //날짜를 출력

		setTimeout(printClock, 1000); // 1초마다 printClock() 함수 호출
	}

	function addZeros(num, digit) { 
		var zero = '';
		num = num.toString();

		if (num.length < digit) {
			for (i = 0; i < digit - num.length; i++) {
				zero += '0';
			}
		}

		return zero + num;
	}
</script>
</body>

</html>