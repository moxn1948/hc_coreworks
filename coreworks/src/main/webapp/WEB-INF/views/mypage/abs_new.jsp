<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- 
	파일설명 : 부재설정-부재이력 페이지
--%>
<jsp:include page="../inc/myPage_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_sh.css">
		<div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>부재설정</h2></div>

                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <form>
	                        <div class="tbl_wrap">
		                            <table class="tbl_ctn abs_set">
		                           		<colgroup>
		                                    <col style="width: 16%;">
		                                    <col style="width: 84%;">
		                                </colgroup>
			                        	<tr>
			                                <td>부재기간</td>
			                                <td>
			                                	<div class="abs_peroid">
			                                		<div class="peroid part1">
		                                                <input type="date" name="" id="">
		                                                
		                                                <select>
		                                                	<c:forEach var="i" begin="00" end="23" step="1">
			                                                	<c:choose>
			                                                		<c:when test="${ i<10 }">
			                                                			<option value="${ i }">0${ i }</option>
			                                                		</c:when>
			                                                		<c:otherwise>
			                                                			<option value="${ i }">${ i }</option>
			                                                		</c:otherwise>
			                                                	</c:choose>
		                                                	</c:forEach>
		                                                </select>
		                                                <select>
		                                                	<c:forEach var="i" begin="00" end="59" step="1">
		                                                		<c:choose>
			                                                		<c:when test="${ i<10 }">
			                                                			<option value="${ i }">0${ i }</option>
			                                                		</c:when>
			                                                		<c:otherwise>
			                                                			<option value="${ i }">${ i }</option>
			                                                		</c:otherwise>
			                                                	</c:choose>
		                                                	</c:forEach>
		                                                </select>
			                                		</div> <!-- part1 End -->
			                                		~
			                                		<div class="peroid part2">
		                                                <input type="date" name="" id="">
		                                                <select>
		                                                	<c:forEach var="i" begin="00" end="23" step="1">
			                                                	<c:choose>
			                                                		<c:when test="${ i<10 }">
			                                                			<option value="${ i }">0${ i }</option>
			                                                		</c:when>
			                                                		<c:otherwise>
			                                                			<option value="${ i }">${ i }</option>
			                                                		</c:otherwise>
			                                                	</c:choose>
		                                                	</c:forEach>
		                                                </select>
		                                                <select>
		                                                	<c:forEach var="i" begin="00" end="59" step="1">
		                                                		<c:choose>
			                                                		<c:when test="${ i<10 }">
			                                                			<option value="${ i }">0${ i }</option>
			                                                		</c:when>
			                                                		<c:otherwise>
			                                                			<option value="${ i }">${ i }</option>
			                                                		</c:otherwise>
			                                                	</c:choose>
		                                                	</c:forEach>
		                                                </select>
			                                		</div> <!-- part2 End -->
			                                	</div> <!-- abs_peroid End -->
			                                </td>
			                            </tr>
			                            <tr>
			                                <td>부재사유</td>
			                                <td>
			                                	<select>
			                                		<option value="edu">교육</option>
		                                            <option value="busTrip">출장</option>
		                                            <option value="outing">외출</option>
		                                            <option value="vacation">휴가</option>
		                                            <option value="earLeave">조퇴</option>
		                                            <option value="sicLeave">병가</option>
		                                            <option value="pubVacation">공가</option>
		                                            <option value="speVacation">특별휴가</option>
		                                            <option value="Absenteeism">결근</option>
		                                            <option value="absLeave">휴직</option>
		                                            <option value="retirement">퇴직</option>
			                                	</select>
			                                </td>
			                            </tr>
			                            <tr>
			                                <td>대리 결재자 유무</td>
			                                <td>
			                                	<div class="presence">
			                                        <input type="radio" name="presence" value="yes" id="yes" checked>
			                                        <label for="yes">유</label>
			                                        <input type="radio" name="presence" value="no" id="no">
			                                        <label for="no">무</label>
		                                        </div>
		                                        <div class="add_presence">
		                                        	<button type="button" class="btn_main btn_presence">대리 결재자</button>
		                                        	<input type="text" name="addPresence" readonly>
		                                        </div>
		                                    </td>
			                            </tr>
			                            <tr>
			                                <td>참고사항</td>
			                                <td>
			                                	<textarea class="note" name="note"></textarea>
			                                </td>
			                            </tr>
		                            </table>
	                        </div>
		                            <div id="btnSet" class="btnSet">
	                        			<button type="button" class="btn_white abs_cancel">취소</button>
		                            	<button type="submit" class="btn_main">등록</button>
		                            </div>
                        </form>
                    </div>
                    <!-- 기본 테이블 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>

<!-- 결재이력상세보기 팝업 -->
<div id="popListAbsence" class="modal">
	<jsp:include page="mypage_pop_list_abs.jsp" />
</div>

<!-- 대결자 상세 팝업 -->
<div id="popDetailAbsence" class="modal">
	<jsp:include page="mypage_pop_detail_abs.jsp" />
</div>


<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        // $("#nav .nav_list").eq(5).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(1).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(1).find(".sub_menu_list").eq(0).addClass("on");
        
        // 메뉴 밑줄 제거
        $('a').css('text-decoration', 'none');
        
        $('.tbl_ctn').css('text-align-last', 'left');
    });
</script>
</body>
</html>
