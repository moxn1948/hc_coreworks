<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- 
	파일설명 : 내 연차 현황
--%>

<jsp:include page="../inc/myPage_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_sh.css">
		<div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>내 연차 현황</h2></div>
                    
                    <!-- 년도 선택 -->
                    <div class="year">
                    	<a class="past"><</a>
                    	<b>2020</b>
                    	<a class="futuer">></a>
                    </div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="tbl_common tbl_basic annual_status">
                    	<div class="tbl_wrap">
                            <table class="tbl_ctn">
	                                <tr>
										<th>총 연차</th>
										<th>사용연차</th>
										<th>추가연차</th>
										<th>잔여연자</th>
	                                </tr>
	                                <tr>
	                                	<td>10</td>
	                                	<td>1</td>
	                                	<td>2.5</td>
	                                	<td>11.5</td>
	                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn">
                                <tr>
                                	<th>시작일</th>
                                	<th>종료일</th>
                                	<th>사용일수</th>
                                </tr>
	                            <c:forEach var="i" begin="0" end="14" step="1">
	                                <tr>
	                                	<td>20/01/01</td>
	                                	<td>20/01/01</td>
	                                	<td><a href="#popDetailAnnual" rel="modal:open">1</a></td>
	                                </tr>
	                            </c:forEach>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                    <div class="pager_wrap">
                        <ul class="pager_cnt clearfix">
                        <li class="pager_com pager_arr first"><a href="javascrpt: void(0);">&#x003C;&#x003C;</a></li>
                        <li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">1</a></li>
                        <li class="pager_com pager_num on"><a href="javascrpt: void(0);">2</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">3</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">4</a></li>
                        <li class="pager_com spager_num"><a href="javascrpt: void(0);">5</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">6</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">7</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">8</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">9</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">10</a></li>
                        <li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
                        <li class="pager_com pager_arr end"><a href="javascrpt: void(0);">&#x003E;&#x003E;</a></li>
                        </ul>
                    </div>
                    <!-- 페이저 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>

<!-- popup include -->
<div id="popDetailAnnual" class="modal">
	<jsp:include page="mypage_pop_ann.jsp" />
</div>


<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        // $("#nav .nav_list").eq(5).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        //$("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        $("#menu_area .menu_list").eq(0).addClass("on").addClass("open");
        $("#menu_area .menu_list").eq(0).find(".sub_menu_list").eq(0).addClass("on");
        
        // 메뉴 밑줄 제거
        $('a').css('text-decoration', 'none');
    });
</script>
</body>
</html>
