<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<h2 class="main_tit filebox_title">사진/파일 모아보기</h2>

<div class="file_imgbox">
	<!-- <div class="filediv">
		<div class="fileimgdiv">
			<i class="far fa-file"></i><br />
		</div>
		<span>첨부파일</span>
	</div> -->
	<div class="thumbnail-wrapper">
		<div class="thumbnail">
			<div class="centered-icon">
				<i class="far fa-file"></i><br />
			</div>
			<div class="centered-icon">
				<span>첨부파일</span>
			</div>
		</div>
	</div>
	<div class="thumbnail-wrapper">
		<div class="thumbnail">
			<div class="centered">
				<img src="${ contextPath }/resources/images/bapbird.jpeg"/>
			</div>
		</div>
	</div>
	<div class="thumbnail-wrapper">
		<div class="thumbnail">
			<div class="centered">
				<img src="${ contextPath }/resources/images/bapbird.jpeg"/>
			</div>
		</div>
	</div>
	<div class="thumbnail-wrapper">
		<div class="thumbnail">
			<div class="centered">
				<img src="${ contextPath }/resources/images/user_img.jpg"/>
			</div>
		</div>
	</div>
	<div class="thumbnail-wrapper">
		<div class="thumbnail">
			<div class="centered">
				<img src="${ contextPath }/resources/images/user_img.jpg"/>
			</div>
		</div>
	</div>
	<div class="thumbnail-wrapper">
		<div class="thumbnail">
			<div class="centered">
				<img src="${ contextPath }/resources/images/user_img.jpg"/>
			</div>
		</div>
	</div>
	<div class="thumbnail-wrapper">
		<div class="thumbnail">
			<div class="centered">
				<img src="${ contextPath }/resources/images/user_img.jpg"/>
			</div>
		</div>
	</div>
	<div class="thumbnail-wrapper">
		<div class="thumbnail">
			<div class="centered">
				<img src="${ contextPath }/resources/images/user_img.jpg"/>
			</div>
		</div>
	</div>
	<div class="thumbnail-wrapper">
		<div class="thumbnail">
			<div class="centered">
				<img src="${ contextPath }/resources/images/user_img.jpg"/>
			</div>
		</div>
	</div>
	<div class="thumbnail-wrapper">
		<div class="thumbnail">
			<div class="centered">
				<img src="${ contextPath }/resources/images/user_img.jpg"/>
			</div>
		</div>
	</div>
	<%-- <div class="filediv">
		<img src="${ contextPath }/resources/images/bapbird.jpeg"/>
	</div>
	<div class="filediv">
		<img src="${ contextPath }/resources/images/bapbird.jpeg"/>
	</div>
	<div class="filediv">
		<img src="${ contextPath }/resources/images/bapbird.jpeg"/>
	</div>
	<div class="filediv">
		<img src="${ contextPath }/resources/images/bapbird.jpeg"/>
	</div>
	<div class="filediv">
		<img src="${ contextPath }/resources/images/bapbird.jpeg"/>
	</div>
	<div class="filediv">
		<img src="${ contextPath }/resources/images/bapbird.jpeg"/>
	</div>
	<div class="filediv">
		<img src="${ contextPath }/resources/images/bapbird.jpeg"/>
	</div>
	<div class="filediv">
		<img src="${ contextPath }/resources/images/bapbird.jpeg"/>
	</div>
	<div class="filediv">
		<img src="${ contextPath }/resources/images/user_img.jpg"/>
	</div>
	<div class="filediv">
		<img src="${ contextPath }/resources/images/user_img.jpg"/>
	</div>
	<div class="filediv">
		<img src="${ contextPath }/resources/images/user_img.jpg" id="test"/>
	</div> --%>
</div>

<button class="btn_main"><a href="#" rel="modal:close">Close</a></button>